## Test Users
| role  | login                          | password  |
| :---  | :---                           | :---      |
| admin | `test_ueoppni_admin@tfbnw.net` | `qwe123r` |
| user  | `test_radjvzv_user@tfbnw.net`  | `qw123e`  |