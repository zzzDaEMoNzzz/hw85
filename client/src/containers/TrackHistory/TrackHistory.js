import React, {Component} from 'react';
import {connect} from "react-redux";
import {getTrackHistory} from "../../store/actions/trackHistoryActions";
import moment from 'moment';
import './TrackHistory.css';

class TrackHistory extends Component {
  componentDidMount() {
    this.props.getTrackHistory();
  }

  render() {
    const history = this.props.trackHistory.map(item => {
      let trackName = '';

      if (item.error) {
        trackName = item.error;
      } else {
        trackName = `${item.track.artist} - ${item.track.name}`;
      }

      return (
        <div key={item._id} className={item.error ? 'TrackHistory-error' : ''}>
          <span className="dateTime">{moment(item.datetime).format('DD/MM/YYYY HH:mm:ss')}</span>
          <span>{trackName}</span>
        </div>
      );
    });

    return (
      <div className="TrackHistory">
        <h2>Track History</h2>
        {history.length > 0 ? history : <div>Nothing to show</div>}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  trackHistory: state.trackHistory.trackHistory
});

const mapDispatchToProps = dispatch => ({
  getTrackHistory: () => dispatch(getTrackHistory())
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);