import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {cleanAlbumsData, deleteAlbum, fetchAlbums, toggleAlbumPublished} from "../../store/actions/albumsActions";
import {fetchArtists} from "../../store/actions/artistsActions";
import Album from "./Album/Album";

class Albums extends Component {
  componentDidMount() {
    if (!this.props.artists || this.props.artists.length === 0) {
      this.props.getArtists();
    }

    this.props.getAlbums(this.props.match.params.artist);
  }

  componentWillUnmount() {
    this.props.cleanAlbumsData();
  }

  render() {
    const albumsList = this.props.albums.map(albumData => (
      <Album
        key={albumData._id}
        data={albumData}
        user={this.props.user}
        togglePublished={this.props.togglePublished}
        deleteAlbum={this.props.deleteAlbum}
      />
    ));

    const artist = this.props.artists.find(artist => artist._id === this.props.match.params.artist);
    const artistName = artist ? artist.name : '';

    if (!artistName) {
      return <div>Artist not found</div>;
    }

    return (
      <Fragment>
        <h2>{artistName}</h2>
        {albumsList}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  albums: state.albums.albums,
  artists: state.artists.artists,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getAlbums: artistID => dispatch(fetchAlbums(artistID)),
  cleanAlbumsData: () => dispatch(cleanAlbumsData()),
  getArtists: () => dispatch(fetchArtists()),
  togglePublished: (albumID, artistID) => dispatch(toggleAlbumPublished(albumID, artistID)),
  deleteAlbum: (albumID, artistID) => dispatch(deleteAlbum(albumID, artistID))
});

export default connect(mapStateToProps, mapDispatchToProps)(Albums);