import React from 'react';
import {apiURL} from "../../../constants";
import {Link} from "react-router-dom";
import './Album.css';
import PublishedStatus from "../../../components/PublishedStatus/PublishedStatus";
import DeleteBtn from "../../../components/DeleteBtn/DeleteBtn";

const Album = ({data, user, togglePublished, deleteAlbum}) => {
  return (
    <Link to={`/albums/${data._id}`} className="Album">
      <img src={`${apiURL}/uploads/${data.image}`} alt=""/>
      <div>
        <h4>
          {data.name}
          {user && (
            <PublishedStatus
              published={data.published}
              onClick={user.role === 'admin' ? () => togglePublished(data._id, data.artist) : null}
            />
          )}
          {user && user.role === 'admin' && (
            <DeleteBtn onClick={() => deleteAlbum(data._id, data.artist)}/>
          )}
        </h4>
        <p>Year: <strong>{data.date}</strong></p>
        <p>Tracks: <strong>{data.tracksCount}</strong></p>
      </div>
    </Link>
  );
};

export default Album;
