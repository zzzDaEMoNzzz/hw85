import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import AddForm from "../../components/AddForm/AddForm";
import {addArtist} from "../../store/actions/artistsActions";

class AddArtist extends Component {
  state = {
    name: {
      value: '',
      required: true
    },
    description: {
      value: '',
      type: 'textarea'
    },
    image: {
      value: null,
      type: 'file',
      required: true
    }
  };

  inputChangeHandler = event => {
    if (typeof this.state[event.target.name] === 'object') {
      this.setState({
        [event.target.name]: {...this.state[event.target.name], value: event.target.value}
      });
    } else {
      this.setState({[event.target.name]: event.target.value});
    }
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: {...this.state[event.target.name], value: event.target.files[0]}
    });
  };

  onSubmit = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key].value);
    });

    this.props.addArtist(formData);
  };

  render() {
    return (
      <Fragment>
        <h2>New artist</h2>
        <AddForm
          data={this.state}
          onChange={this.inputChangeHandler}
          onChangeFile={this.fileChangeHandler}
          onSubmit={this.onSubmit}
        />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addArtist: artistData => dispatch(addArtist(artistData))
});

export default connect(null, mapDispatchToProps)(AddArtist);