import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Modal from 'react-responsive-modal';
import Track from "./Track/Track";
import {
  cleanAlbumInfo,
  deleteTrack,
  fetchAlbumInfo,
  fetchTracks,
  toggleTrackPublished
} from "../../store/actions/tracksActions";
import {addToHistory} from "../../store/actions/trackHistoryActions";
import './Tracks.css';

class Tracks extends Component {
  state = {
    showModal: false,
    modalContent: null
  };

  componentDidMount() {
    const albumID = this.props.match.params.id;

    this.props.getTracks(albumID);
    this.props.getAlbumInfo(albumID)
  }

  componentWillUnmount() {
    this.props.cleanAlbumInfo();
  }

  showModal = content => {
    this.setState({
      showModal: true,
      modalContent: content
    });
  };

  hideModal = () => {
    this.setState({
      showModal: false,
      modalContent: null
    });
  };

  render() {
    const tracksList = this.props.tracks.map(trackData => (
      <Track
        key={trackData._id}
        data={trackData}
        onClick={this.props.user ? this.props.addToHistory : null}
        showModal={this.showModal}
        user={this.props.user}
        togglePublished={this.props.togglePublished}
        deleteTrack={this.props.deleteTrack}
      />
    ));

    const artistName = this.props.albumInfo.artist ? this.props.albumInfo.artist.name : '';
    const albumName = this.props.albumInfo.name || '';

    if (!artistName) {
      return <div>Album not found</div>;
    }

    return (
      <Fragment>
        <h2>{`${artistName} - ${albumName}`}</h2>
        <div className="Tracks">{tracksList}</div>
        <Modal
          open={this.state.showModal}
          onClose={this.hideModal}
          classNames={{
            modal: 'Tracks-modal',
            closeButton: 'Tracks-modalBtn'
          }}
          center
        >
          {this.state.modalContent}
        </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  tracks: state.tracks.tracks,
  albumInfo: state.tracks.albumInfo,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getTracks: albumID => dispatch(fetchTracks(albumID)),
  getAlbumInfo: albumID => dispatch(fetchAlbumInfo(albumID)),
  cleanAlbumInfo: () => dispatch(cleanAlbumInfo()),
  addToHistory: trackID => dispatch(addToHistory(trackID)),
  togglePublished: (trackID, albumID) => dispatch(toggleTrackPublished(trackID, albumID)),
  deleteTrack: (trackID, albumID) => dispatch(deleteTrack(trackID, albumID))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);