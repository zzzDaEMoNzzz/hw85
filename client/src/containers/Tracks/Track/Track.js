import React from 'react';
import moment from 'moment';
import YouTube from 'react-youtube';
import './Track.css';
import PublishedStatus from "../../../components/PublishedStatus/PublishedStatus";
import DeleteBtn from "../../../components/DeleteBtn/DeleteBtn";

const Track = ({data, onClick, showModal, user, togglePublished, deleteTrack}) => {
  const trackHasYouTube = !!data.youtube;

  const youtubeBtnHandler = () => {
    const videoId = data.youtube && data.youtube.split('v=')[1];
    const player = <YouTube videoId={videoId}/>;

    showModal(player);
  };

  return (
    <div
      className={"Track " + (onClick ? 'loggedIn' : '')}
      onClick={onClick ? () => onClick(data._id) : null}
    >
      <span>{data.number}.</span>
      <span>{data.name}</span>
      <span>({moment(data.duration * 1000).format('m:ss')})</span>
      {trackHasYouTube && (
        <span>
          <button className="Track-youtubeBtn" onClick={youtubeBtnHandler}>YouTube</button>
        </span>
      )}
      {user && (
        <PublishedStatus
          published={data.published}
          onClick={user.role === 'admin' ? () => togglePublished(data._id, data.album) : null}
        />
      )}
      {user && user.role === 'admin' && (
        <DeleteBtn onClick={() => deleteTrack(data._id, data.album)}/>
      )}
    </div>
  );
};

export default Track;
