import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import AddForm from "../../components/AddForm/AddForm";
import {addAlbum} from "../../store/actions/albumsActions";
import {fetchArtists} from "../../store/actions/artistsActions";

class AddAlbum extends Component {
  state = {
    artist: {
      value: '',
      type: 'select',
      list: [],
      loading: false,
      required: true
    },
    name: {
      value: '',
      required: false
    },
    date: {
      value: '',
      required: true
    },
    image: {
      value: null,
      type: 'file',
      required: true
    }
  };

  componentDidMount() {
    this.setState({artist: {...this.state.artist, loading: true}});

    this.props.getArtists().then(data => {
      const list = data.artists.map(artist => ({
        value: artist._id,
        title: artist.name
      }));

      this.setState({artist: {...this.state.artist, list, loading: false}});
    });
  }

  inputChangeHandler = event => {
    if (typeof this.state[event.target.name] === 'object') {
      this.setState({
        [event.target.name]: {...this.state[event.target.name], value: event.target.value}
      });
    } else {
      this.setState({[event.target.name]: event.target.value});
    }
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: {...this.state[event.target.name], value: event.target.files[0]}
    });
  };

  onSubmit = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key].value);
    });

    this.props.addAlbum(formData);
  };

  render() {
    return (
      <Fragment>
        <h2>New album</h2>
        <AddForm
          data={this.state}
          onChange={this.inputChangeHandler}
          onChangeFile={this.fileChangeHandler}
          onSubmit={this.onSubmit}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  artists: state.artists.artists
});

const mapDispatchToProps = dispatch => ({
  addAlbum: albumData => dispatch(addAlbum(albumData)),
  getArtists: () => dispatch(fetchArtists())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);