import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import AddForm from "../../components/AddForm/AddForm";
import {fetchAlbums} from "../../store/actions/albumsActions";
import {fetchArtists} from "../../store/actions/artistsActions";
import {addTrack} from "../../store/actions/tracksActions";

class AddTrack extends Component {
  state = {
    artist: {
      value: '',
      type: 'select',
      list: [],
      loading: false,
      required: true,
      onChange: artistID => this.getAlbums(artistID)
    },
    album: {
      value: '',
      type: 'select',
      list: [],
      loading: false,
      required: true,
      reference: 'artist'
    },
    number: {
      value: '',
      required: true
    },
    name: {
      value: '',
      required: true
    },
    duration: {
      value: '',
      required: true
    },
    youtube: ''
  };

  componentDidMount() {
    this.setState({artist: {...this.state.artist, loading: true}});

    this.props.getArtists().then(data => {
      const list = data.artists.map(artist => ({
        value: artist._id,
        title: artist.name
      }));

      this.setState({artist: {...this.state.artist, list, loading: false}});
    });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.albums !== this.props.albums) {
      const list = this.props.albums.map(album => ({
        value: album._id,
        title: album.name
      }));

      this.setState({album: {...this.state.album, list, loading: false}});
    }
  }

  inputChangeHandler = event => {
    if (typeof this.state[event.target.name] === 'object') {
      this.setState({
        [event.target.name]: {...this.state[event.target.name], value: event.target.value}
      });
    } else {
      this.setState({[event.target.name]: event.target.value});
    }
  };

  getAlbums(artistID) {
    this.setState({album: {...this.state.album, loading: true}});
    this.props.getAlbums(artistID);
  }

  onSubmit = event => {
    event.preventDefault();

    const trackData = {};

    Object.keys(this.state).forEach(key => {
      if (typeof this.state[key] === 'object') {
        trackData[key] = this.state[key].value;
      } else {
        trackData[key] = this.state[key];
      }
    });

    this.props.addTrack(trackData);
  };

  render() {
    return (
      <Fragment>
        <h2>New track</h2>
        <AddForm
          data={this.state}
          onChange={this.inputChangeHandler}
          onSubmit={this.onSubmit}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  artists: state.artists.artists,
  albums: state.albums.albums
});

const mapDispatchToProps = dispatch => ({
  addTrack: trackData => dispatch(addTrack(trackData)),
  getArtists: () => dispatch(fetchArtists()),
  getAlbums: artistID => dispatch(fetchAlbums(artistID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTrack);