import React from 'react';
import {apiURL} from "../../../constants";
import {Link} from "react-router-dom";
import './Artist.css';
import PublishedStatus from "../../../components/PublishedStatus/PublishedStatus";
import DeleteBtn from "../../../components/DeleteBtn/DeleteBtn";

const Artist = ({data, user, togglePublished, deleteArtist}) => {
  return (
    <Link to={`/artists/${data._id}`} className="Artist">
      <img src={`${apiURL}/uploads/${data.image}`} alt=""/>
      <div>
        <h4>
          {data.name}
          {user && (
            <PublishedStatus
              published={data.published}
              onClick={user.role === 'admin' ? () => togglePublished(data._id) : null}
            />
          )}
          {user && user.role === 'admin' && (
            <DeleteBtn onClick={() => deleteArtist(data._id)}/>
          )}
        </h4>
        <p>{data.description}</p>
      </div>
    </Link>
  );
};

export default Artist;
