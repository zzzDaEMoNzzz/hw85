import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteArtist, fetchArtists, toggleArtistPublished} from "../../store/actions/artistsActions";
import Artist from "./Artist/Artist";

class Artists extends Component {
  componentDidMount() {
    this.props.getArtists();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.user !== this.props.user) {
      this.props.getArtists();
    }
  }

  render() {
    const artistsList = this.props.artists.map(artistData => (
      <Artist
        key={artistData._id}
        data={artistData}
        user={this.props.user}
        togglePublished={this.props.togglePublished}
        deleteArtist={this.props.deleteArtist}
      />
    ));

    return (
      <Fragment>
        <h2>Artists</h2>
        {artistsList}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  artists: state.artists.artists,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getArtists: () => dispatch(fetchArtists()),
  togglePublished: artistID => dispatch(toggleArtistPublished(artistID)),
  deleteArtist: artistID => dispatch(deleteArtist(artistID))
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);