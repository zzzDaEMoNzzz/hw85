import {CLEAN_ALBUM_INFO, FETCH_ALBUM_INFO_SUCCESS, FETCH_TRACKS_SUCCESS} from "../actions/tracksActions";

const initialState = {
  tracks: [],
  albumInfo: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRACKS_SUCCESS:
      return {...state, tracks: action.tracks};
    case FETCH_ALBUM_INFO_SUCCESS:
      return {...state, albumInfo: action.albumInfo};
    case CLEAN_ALBUM_INFO:
      return {...state, tracks: [], albumInfo: {}};
    default:
      return state;
  }
};

export default reducer;