import axios from '../../axios-api';

export const addToHistory = trackID => {
  return dispatch => {
    return axios.post('/track_history', {track: trackID});
  }
};

export const GET_TRACK_HISTORY_SUCCESS = 'GET_TRACK_HISTORY_SUCCESS';

const getTrackHistorySuccess = trackHistory => ({type: GET_TRACK_HISTORY_SUCCESS, trackHistory});

export const getTrackHistory = () => {
  return dispatch => {
    return axios.get('/track_history').then(
      response => dispatch(getTrackHistorySuccess(response.data))
    );
  }
};