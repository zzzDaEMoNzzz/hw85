import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router';

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, artists});

export const fetchArtists = () => {
  return dispatch => {
    return axios.get('/artists').then(
      response => dispatch(fetchArtistsSuccess(response.data))
    );
  }
};

export const addArtist = artistData => {
  return dispatch => {
    axios.post('/artists', artistData).then(
      () => {
        NotificationManager.success('Artist added');
        dispatch(push('/'));
      },
      error => {
        if (error.response) {
          const errorsObject = error.response.data && error.response.data.errors;
          const errorMessage = errorsObject[Object.keys(errorsObject)[0]].message;

          if (errorMessage) {
            NotificationManager.error(errorMessage);
          } else {
            NotificationManager.error('Artist adding failed');
          }
        } else {
          NotificationManager.error('No connection');
        }
      }
    );
  };
};

export const toggleArtistPublished = artistID => {
  return dispatch => {
    axios.post(`/artists/${artistID}/toggle_published`).then(
      () => dispatch(fetchArtists())
    );
  };
};

export const deleteArtist = artistID => {
  return dispatch => {
    axios.delete(`/artists/${artistID}`).then(
      () => {
        NotificationManager.success('Artist successfully deleted');
        dispatch(fetchArtists());
      }
    );
  };
};