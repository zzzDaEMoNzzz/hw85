import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router';

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});

export const fetchAlbums = artistID => {
  return dispatch => {
    return axios.get(`/albums/?artist=${artistID}`).then(
      response => dispatch(fetchAlbumsSuccess(response.data))
    );
  }
};

export const CLEAN_ALBUMS_DATA = 'CLEAN_ALBUMS_DATA';

export const cleanAlbumsData = () => ({type: CLEAN_ALBUMS_DATA});

export const addAlbum = albumData => {
  return dispatch => {
    axios.post('/albums', albumData).then(
      response => {
        NotificationManager.success('Album added');
        dispatch(push(`/artists/${response.data.artist}`));
      },
      error => {
        if (error.response) {
          const errorsObject = error.response.data && error.response.data.errors;
          const errorMessage = errorsObject[Object.keys(errorsObject)[0]].message;

          if (errorMessage) {
            NotificationManager.error(errorMessage);
          } else {
            NotificationManager.error('Album adding failed');
          }
        } else {
          NotificationManager.error('No connection');
        }
      }
    );
  };
};

export const toggleAlbumPublished = (albumID, artistID) => {
  return dispatch => {
    axios.post(`/albums/${albumID}/toggle_published`).then(
      () => dispatch(fetchAlbums(artistID))
    );
  };
};

export const deleteAlbum = (albumID, artistID) => {
  return dispatch => {
    axios.delete(`/albums/${albumID}`).then(
      () => {
        NotificationManager.success('Album successfully deleted');
        dispatch(fetchAlbums(artistID));
      }
    );
  };
};