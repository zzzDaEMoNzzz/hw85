import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router';

export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';

const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});

export const fetchTracks = albumID => {
  return dispatch => {
    return axios.get(`/tracks/?album=${albumID}`).then(
      response => dispatch(fetchTracksSuccess(response.data))
    );
  }
};

export const FETCH_ALBUM_INFO_SUCCESS = 'FETCH_ALBUM_INFO_SUCCESS';

const fetchAlbumInfoSuccess = albumInfo => ({type: FETCH_ALBUM_INFO_SUCCESS, albumInfo});

export const fetchAlbumInfo = albumID => {
  return dispatch => {
    return axios.get(`/albums/${albumID}`).then(
      response => dispatch(fetchAlbumInfoSuccess(response.data))
    );
  }
};

export const CLEAN_ALBUM_INFO = 'CLEAN_ALBUM_INFO';

export const cleanAlbumInfo = () => ({type: CLEAN_ALBUM_INFO});

export const addTrack = trackData => {
  return dispatch => {
    axios.post('/tracks', trackData).then(
      response => {
        NotificationManager.success('Track added');
        dispatch(push(`/albums/${response.data.album}`));
      },
      error => {
        if (error.response) {
          const errorsObject = error.response.data && error.response.data.errors;
          const errorMessage = errorsObject[Object.keys(errorsObject)[0]].message;

          if (errorMessage) {
            NotificationManager.error(errorMessage);
          } else {
            NotificationManager.error('Artist adding failed');
          }
        } else {
          NotificationManager.error('No connection');
        }
      }
    );
  };
};

export const toggleTrackPublished = (trackID, albumID) => {
  return dispatch => {
    axios.post(`/tracks/${trackID}/toggle_published`).then(
      () => dispatch(fetchTracks(albumID))
    );
  };
};

export const deleteTrack = (trackID, albumID) => {
  return dispatch => {
    axios.delete(`/tracks/${trackID}`).then(
      () => {
        NotificationManager.success('Track successfully deleted');
        dispatch(fetchTracks(albumID));
      }
    );
  };
};