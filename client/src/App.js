import React, { Component } from 'react';
import {connect} from "react-redux";
import {NotificationContainer} from "react-notifications";
import './App.css';
import Routes from "./Routes";
import Header from "./components/Header/Header";
import {checkAuth, logoutUser} from "./store/actions/usersActions";

class App extends Component {
  componentDidMount() {
    if (this.props.user) {
      this.props.checkAuth();
    }
  }

  render() {
    return (
      <div className="App">
        <NotificationContainer/>
        <Header
          user={this.props.user}
          logoutUser={this.props.logoutUser}
        />
        <div className="App-body">
          <Routes user={this.props.user}/>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser()),
  checkAuth: () => dispatch(checkAuth())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
