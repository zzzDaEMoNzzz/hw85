import React, {Component} from 'react';
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props'
import {connect} from "react-redux";
import {facebookLogin} from "../../store/actions/usersActions";
import {NotificationManager} from "react-notifications";
import './FacebookLogin.css';

class FacebookLogin extends Component {
  facebookLogin = data => {
    if (data.error) {
      NotificationManager.error('Something went wrong');
    } else if (data.name) {
      this.props.facebookLogin(data);
    }
  };

  render() {
    return (
      <FacebookLoginButton
        appId="474691306603779"
        callback={this.facebookLogin}
        fields="name,email,picture"
        render={renderProps => (
          <button onClick={renderProps.onClick} className="FacebookLogin">
            Sign in with Facebook
          </button>
        )}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null, mapDispatchToProps)(FacebookLogin);