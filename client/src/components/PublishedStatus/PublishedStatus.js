import React from 'react';
import './PublishedStatus.css';

const PublishedStatus = ({published, onClick}) => {
  const togglePublished = event => {
    event.preventDefault();
    event.stopPropagation();

    onClick();
  };

  const classes = ['PublishedStatus'];

  if (!published) {
    classes.push('unpublished');
  }

  if (!onClick && published) {
    classes.push('hidden');
  }

  let status = '';

  if (onClick) {
    status = published ? 'Unpublish' : 'Publish';
  } else {
    status = published ? 'Published' : 'Unpublished';
  }

  return (
    <div
      className={classes.join(' ')}
      onClick={onClick ? togglePublished : null}
      hidden={!onClick && published}
      style={!onClick && published ? {display: 'none'} : null}
    >
      {status}
    </div>
  );
};

export default PublishedStatus;
