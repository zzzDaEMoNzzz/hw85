import React, {Fragment} from 'react';
import {Link} from "react-router-dom";

const Dropdown = ({show, onClick, userAvatar, userName, logout}) => {
  return (
    <Fragment>
      <div className="Header-userWrapper">
        <img src={userAvatar} alt=""/>
        <button
          className={"Header-user " + (show ? "arrowUp" : "arrowDown")}
          onClick={onClick}
        >
          {userName}
        </button>
      </div>
      <div
        className="Header-dropDown"
        style={{display: show ? 'flex' : 'none'}}
      >
        <Link to="/artists/add">Add artist</Link>
        <Link to="/albums/add">Add album</Link>
        <Link to="/tracks/add">Add track</Link>
        <Link to="/tracks/history">Track History</Link>
        <Link to="/" onClick={logout}>Logout</Link>
      </div>
    </Fragment>
  );
};

export default Dropdown;
