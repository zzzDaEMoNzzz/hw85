import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import './Header.css';
import Dropdown from './Dropdown/Dropdown';
import withoutAvatarImg from '../../assetts/images/noAvatar.png';
import {apiURL} from "../../constants";

class Header extends Component {
  state = {
    showMenu: false
  };

  toggleMenu = () => {
    if (!this.state.showMenu) {
      this.setState({showMenu: true});
      document.addEventListener('click', this.toggleMenu);
    } else {
      this.setState({showMenu: false});
      document.removeEventListener('click', this.toggleMenu);
    }
  };

  getAvatarUrl = url => {
    return url.includes('//') ? url : `${apiURL}/uploads/${url}`;
  };

  render() {
    let authentication = (
      <nav>
        <NavLink to="/register">Sign Up</NavLink>
        <NavLink to="/login">Login</NavLink>
      </nav>
    );

    let avatar = withoutAvatarImg;

    if (this.props.user && this.props.user.avatar) {
      avatar = this.getAvatarUrl(this.props.user.avatar);
    }

    const userMenu = (
      <Dropdown
        show={this.state.showMenu}
        onClick={this.toggleMenu}
        userAvatar={avatar}
        userName={this.props.user && this.props.user.displayName}
        logout={this.props.logoutUser}
      />
    );

    return (
      <header className="Header">
        <Link to="/" className="Header-logo">Music</Link>
        {this.props.user ? userMenu : authentication}
      </header>
    );
  }
}

export default Header;