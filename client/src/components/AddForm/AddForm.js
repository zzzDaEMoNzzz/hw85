import React, {Component} from 'react';
import './AddForm.css';
import FormElement from "../FormElement/FormElement";

class AddForm extends Component {
  render() {
    return (
      <form className="AddForm" onSubmit={this.props.onSubmit}>
        {Object.keys(this.props.data).map(key => {
          const props = {
            key: key,
            propertyName: key,
            title: key[0].toUpperCase() + key.slice(1),
            value: typeof this.props.data[key] === 'object' ? this.props.data[key].value : this.props.data[key],
            type: this.props.data[key].type,
            list: this.props.data[key].list,
            loading: this.props.data[key].loading,
            onChange: this.props.onChange,
            onChangeFile: this.props.onChangeFile,
            required: this.props.data[key].required || false
          };

          if (this.props.data[key].onChange) {
            props.onChange = event => {
              this.props.onChange(event);
              this.props.data[key].onChange(event.target.value);
            };
          }

          if (this.props.data[key].reference) {
            props.reference = {
              key: this.props.data[key].reference,
              ...this.props.data[this.props.data[key].reference]
            };
          }

          return (
            <FormElement {...props}/>
          );
        })}
        <button>Send</button>
      </form>
    );
  }
}

export default AddForm;