import React from 'react';
import './DeleteBtn.css';

const DeleteBtn = ({onClick}) => {
  const onClickHandler = event => {
    event.preventDefault();
    event.stopPropagation();

    if (window.confirm('Are you sure?')) {
      onClick();
    }
  };

  return (
    <div className="DeleteBtn" onClick={onClickHandler}>Delete</div>
  );
};

export default DeleteBtn;