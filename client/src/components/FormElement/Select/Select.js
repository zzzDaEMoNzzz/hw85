import React from 'react';

const Select = props => {
  const selectClasses = [];

  if (props.loading) {
    selectClasses.push('FormElement-loading');
  }

  if (props.list.length === 0) {
    selectClasses.push('FormElement-hideArrow ');
  }

  let selectPlaceholder = `Please select ${props.propertyName}`;

  if (props.reference && props.reference.value === '') {
    selectPlaceholder = `Waiting for ${props.reference.key}`;
  }
  
  return (
    <select
      id={props.propertyName}
      name={props.propertyName}
      value={props.value}
      onChange={props.onChange}
      required={props.required}
      disabled={props.list.length === 0}
      className={selectClasses.join(' ')}
      style={props.value === '' ? {color: '#555'} : null}
    >
      <option value='' hidden>{selectPlaceholder}</option>
      {props.list.map(listItem => (
        <option key={listItem.value} value={listItem.value}>{listItem.title}</option>
      ))}
    </select>
  );
};

export default Select;