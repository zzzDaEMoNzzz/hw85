import axios from 'axios';
import {apiURL} from "./constants";
import store from "./store/configureStore";

const instance = axios.create({
  baseURL: apiURL
});

instance.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = store.getState().users.user.token;
  } catch (e) {
    //do nothing, user is not logged in
  }

  return config;
});

export default instance;
