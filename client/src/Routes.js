import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import Artists from "./containers/Artists/Artists";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import AddArtist from "./containers/AddArtist/AddArtist";
import AddAlbum from "./containers/AddAlbum/AddAlbum";
import AddTrack from "./containers/AddTrack/AddTrack";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props}/> : <Redirect to="/login"/>;
};

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={Artists}/>
      <Route path="/artists" exact component={Artists}/>
      <ProtectedRoute
        isAllowed={!!user}
        path="/artists/add"
        exact
        component={AddArtist}
      />
      <Route path="/artists/:artist" exact component={Albums}/>
      <ProtectedRoute
        isAllowed={!!user}
        path="/albums/add"
        exact
        component={AddAlbum}
      />
      <Route path="/albums/:id" exact component={Tracks}/>
      <ProtectedRoute
        isAllowed={!!user}
        path="/tracks/add"
        exact
        component={AddTrack}
      />
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
      <ProtectedRoute
        isAllowed={!!user}
        path="/tracks/history"
        exact
        component={TrackHistory}
      />
      <Route render={() => <h2>Page not found...</h2>}/>
    </Switch>
  );
};

export default Routes;
