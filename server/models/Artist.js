const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ArtistSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  image: {
    type: String,
    required: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  published: {
    type: Boolean,
    required: true,
    default: false
  }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;