const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackSchema = new Schema({
  number: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  album: {
    type: Schema.Types.ObjectId,
    ref: 'Album',
    required: true
  },
  youtube: String,
  duration: {
    type: Number,
    required: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  published: {
    type: Boolean,
    required: true,
    default: false
  }
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;