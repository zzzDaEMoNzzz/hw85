const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const Album = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require('./models/Track');
const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const [user, admin] = await User.create(
    {
      username: 'user',
      password: '123',
      displayName: 'User',
      role: 'user',
      token: nanoid()
    },
    {
      username: 'admin',
      password: '123',
      displayName: 'Admin',
      role: 'admin',
      token: nanoid()
    },
    {
      username: 'test_ueoppni_admin@tfbnw.net',
      displayName: 'Test Admin',
      role: 'admin',
      facebookId: '101132817805231',
      password: nanoid(),
      token: nanoid()
    },
    {
      username: 'test_radjvzv_user@tfbnw.net',
      displayName: 'Test User',
      role: 'user',
      facebookId: '102531377663317',
      password: nanoid(),
      token: nanoid()
    }
  );

  const [neffex, hollywood_undead] = await Artist.create(
    {
      name: 'Neffex',
      description: 'Hip-Hop/Rock band from Los Angeles',
      image: '_artist_neffex.jpg',
      user: user._id
    },
    {
      name: 'Hollywood Undead',
      description: 'Hollywood Undead is an American rap rock band from Los Angeles, California, formed in 2005',
      image: '_artist_hollywood_undead.jpg',
      user: admin._id,
      published: true
    },
  );

  const [bom, destiny, five, dotd] = await Album.create(
    {
      name: 'Best of Me: The Collection',
      artist: neffex._id,
      date: '2019',
      image: '_album_best_of_me.jpg',
      user: user._id
    },
    {
      name: 'Destiny: The Collection',
      artist: neffex._id,
      date: '2019',
      image: '_album_destiny.jpg',
      user: user._id,
      published: true
    },
    {
      name: 'Five',
      artist: hollywood_undead._id,
      date: '2017',
      image: '_album_five.jpg',
      user: user._id,
      published: true
    },
    {
      name: 'Day of the Dead',
      artist: hollywood_undead._id,
      date: '2015',
      image: '_album_day_of_the_dead.jpg',
      user: admin._id
    }
  );

  const tracks = [
    {number: 1, name: 'Best of Me', album: bom._id, duration: 227},
    {number: 2, name: 'Grateful', album: bom._id, duration: 182},
    {number: 3, name: 'Numb', album: bom._id, duration: 144},
    {number: 4, name: 'When Everything Is Gone', album: bom._id, duration: 242},
    {number: 5, name: 'Best of Me (Barren Gates Remix)', album: bom._id, duration: 163},
    {number: 6, name: 'Play', album: bom._id, duration: 135},
    {number: 7, name: 'Flirt', album: bom._id, duration: 159},
    {number: 8, name: 'No Sleep', album: bom._id, duration: 180},
    {number: 9, name: 'Savage', album: bom._id, duration: 173},
    {number: 10, name: 'Light It Up', album: bom._id, duration: 217},
    {number: 11, name: 'One of a Kind', album: bom._id, duration: 175},
    {number: 12, name: 'Dangerous', album: bom._id, duration: 216},
    {number: 13, name: 'Chance', album: bom._id, duration: 217},
    {number: 14, name: 'Alive', album: bom._id, duration: 180},
    {number: 15, name: 'Dance Again', album: bom._id, duration: 196},
    {number: 16, name: 'Pro', album: bom._id, duration: 217},

    {number: 1, name: 'Destiny', album: destiny._id, duration: 206},
    {number: 2, name: 'Cold', album: destiny._id, duration: 186},
    {number: 3, name: 'Destiny (Equanimous Remix)', album: destiny._id, duration: 192},
    {number: 4, name: 'Cold in The Water', album: destiny._id, duration: 164},
    {number: 5, name: 'Life', album: destiny._id, duration: 128},
    {number: 6, name: 'Things Are Gonna Get Better', album: destiny._id, duration: 329},
    {number: 7, name: 'Hope', album: destiny._id, duration: 220},
    {number: 8, name: 'Myself', album: destiny._id, duration: 194},
    {number: 9, name: 'Struggle', album: destiny._id, duration: 165},
    {number: 10, name: 'Nightmare', album: destiny._id, duration: 184},
    {number: 11, name: 'Fear', album: destiny._id, duration: 126},
    {number: 12, name: 'One Shot', album: destiny._id, duration: 185},
    {number: 13, name: 'Deep Thoughts', album: destiny._id, duration: 185},
    {number: 14, name: 'Memories', album: destiny._id, duration: 236},
    {number: 15, name: 'Judge', album: destiny._id, duration: 144},

    {number: 1, name: 'California Dreaming', album: five._id, duration: 234, youtube: 'https://www.youtube.com/watch?v=jjjaU5kQV8k'},
    {number: 2, name: 'Whatever It Takes', album: five._id, duration: 187, youtube: 'https://www.youtube.com/watch?v=teRTjJUc6vo'},
    {number: 3, name: 'Bad Moon', album: five._id, duration: 232},
    {number: 4, name: 'Ghost Beach', album: five._id, duration: 234},
    {number: 5, name: 'Broken Record', album: five._id, duration: 219},
    {number: 6, name: 'Nobody\'s Watching', album: five._id, duration: 238},
    {number: 7, name: 'Renegade', album: five._id, duration: 183},
    {number: 8, name: 'Black Cadillac feat. B-Real', album: five._id, duration: 227},
    {number: 9, name: 'Pray (put em in the dirt)', album: five._id, duration: 264},
    {number: 10, name: 'Cashed Out', album: five._id, duration: 209},
    {number: 11, name: 'Riot', album: five._id, duration: 227},
    {number: 12, name: 'We Own The Night', album: five._id, duration: 242},
    {number: 13, name: 'Bang Bang', album: five._id, duration: 220},
    {number: 14, name: 'Your Life', album: five._id, duration: 205},

    {number: 1, name: 'Usual Suspects', album: dotd._id, duration: 227},
    {number: 2, name: 'How We Roll', album: dotd._id, duration: 285},
    {number: 3, name: 'Day of the Dead', album: dotd._id, duration: 235, youtube: 'https://www.youtube.com/watch?v=bl0e5DrYLyY'},
    {number: 4, name: 'War Child', album: dotd._id, duration: 238},
    {number: 5, name: 'Dark Places', album: dotd._id, duration: 279},
    {number: 6, name: 'Take Me Home', album: dotd._id, duration: 228},
    {number: 7, name: 'Gravity', album: dotd._id, duration: 199},
    {number: 8, name: 'Does Everybody In The World Have To Die', album: dotd._id, duration: 197},
    {number: 9, name: 'Disease', album: dotd._id, duration: 212},
    {number: 10, name: 'Party By Myself', album: dotd._id, duration: 250},
    {number: 11, name: 'Live Forever', album: dotd._id, duration: 220},
    {number: 12, name: 'Save Me', album: dotd._id, duration: 207},
    {number: 13, name: 'Guzzle, Guzzle', album: dotd._id, duration: 219},
    {number: 14, name: 'I\'ll Be There', album: dotd._id, duration: 241},
    {number: 15, name: 'Let Go', album: dotd._id, duration: 256}
  ];

  tracks.map(track => {
    const rnd = Math.floor(Math.random() * 100) + 1;

    track.user = rnd > 50 ? user._id : admin._id;
    track.published = rnd <= 30;

    return track;
  });

  await Track.create(...tracks);

  return connection.close();
};

run().catch(error => {
  console.error('Something wrong happened...', error);
});