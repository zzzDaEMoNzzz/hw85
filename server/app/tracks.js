const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const criteria = require('../middleware/criteria');
const Track = require('../models/Track');
const Album = require('../models/Album');

const router = express.Router();

router.get('/', [auth(false), criteria], async (req, res) => {
  try {
    if (req.query.album) {
      req.criteria.album = req.query.album;
    }

    const tracks = await Track.find(req.criteria, null, {sort: req.query.album ? {number: 'ASC'} : null});

    res.send(tracks);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get('/:artist', (req, res) => {
  Album.find({artist: req.params.artist}).then(albums => {
    const album_ids = albums.map(album => album._id);

    Track.find({album: {$in: album_ids}})
      .then(tracks => res.send(tracks))
      .catch(() => res.sendStatus(500));
  }).catch(() => res.sendStatus(500));
});

router.post('/', auth(), (req, res) => {
  const trackData = {
    number: req.body.number,
    name: req.body.name,
    album: req.body.album,
    duration: req.body.duration,
    user: req.user._id
  };

  if (req.body.youtube) {
    trackData.youtube = req.body.youtube;
  }

  if (req.file) {
    trackData.image = req.file.filename;
  }

  const track = new Track(trackData);

  track.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth(), permit('admin')], (req, res) => {
  Track.findById(req.params.id)
    .then(track => {
      if (track) {
        Track.deleteOne({_id: track._id})
          .then(result => res.send(result))
          .catch(() => res.sendStatus(500));
      } else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/:id/toggle_published', [auth(), permit('admin')], async (req, res) => {
  try {
    const track = await Track.findById(req.params.id);

    if (!track) {
      return res.sendStatus(404);
    }

    track.published = !track.published;

    await track.save();

    return res.send(track);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;