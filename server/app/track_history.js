const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const Track = require('../models/Track');
const Artist = require('../models/Artist');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/', auth(), async (req, res) => {
  try {
    const trackHistoryData = await TrackHistory.find({user: req.user._id}, null, {sort: {datetime: 'DESC'}}).lean();

    const promises = await trackHistoryData.map(async item => {
      try {
        const trackData = await Track.findById(item.track).populate('album').lean();
        const artist = await Artist.findById(trackData.album.artist);

        return {
          ...item,
          track: {
            ...trackData,
            album: trackData.album.name,
            artist: artist.name
          }
        };
      } catch (e) {
        return {...item, error: 'Deleted track'};
      }
    });

    const trackHistory = await Promise.all(promises);

    res.send(trackHistory);
  } catch (error) {
    res.sendStatus(500);
  }
});

router.post('/', auth(), async (req, res) => {
  const trackHistoryData = req.body;
  trackHistoryData.user = req.user._id;
  trackHistoryData.datetime = new Date().toISOString();

  const trackHistory = new TrackHistory(trackHistoryData);

  try {
    await trackHistory.save();
    return res.send(trackHistory);
  } catch (error) {
    return res.status(400).send(error);
  }
});

module.exports = router;