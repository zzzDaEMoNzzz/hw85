const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const criteria = require('../middleware/criteria');
const upload = require('../middleware/upload');
const Album = require('../models/Album');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', [auth(false), criteria], async (req, res) => {
  try {
    if (req.query.artist) {
      req.criteria.artist = req.query.artist;
    }

    const albums = await Album.find(req.criteria, null, {sort: {date: 'ASC'}}).lean();

    const albumsPromises = albums.map(async album => {
      const tracks = await Track.find({album: album._id});
      album.tracksCount = tracks.length || 0;

      return album;
    });

    const result = await Promise.all(albumsPromises);
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
});


router.get('/:id', (req, res) => {
  Album.findById(req.params.id).populate('artist')
    .then(album => {
      if (album) res.send(album);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/', [auth(), upload.single('image')], (req, res) => {
  const albumData = {
    name: req.body.name,
    artist: req.body.artist,
    date: req.body.date,
    user: req.user._id
  };

  if (req.file) {
    albumData.image = req.file.filename;
  }

  const album = new Album(albumData);

  album.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth(), permit('admin')], (req, res) => {
  Album.findById(req.params.id)
    .then(album => {
      if (album) {
        Album.deleteOne({_id: album._id})
          .then(result => res.send(result))
          .catch(() => res.sendStatus(500));
      } else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/:id/toggle_published', [auth(), permit('admin')], async (req, res) => {
  try {
    const album = await Album.findById(req.params.id);

    if (!album) {
      return res.sendStatus(404);
    }

    album.published = !album.published;

    await album.save();

    return res.send(album);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;
