const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const criteria = require('../middleware/criteria');
const upload = require('../middleware/upload');
const Artist = require('../models/Artist');

const router = express.Router();

router.get('/', [auth(false), criteria], async (req, res) => {
  try {
    const artists = await Artist.find(req.criteria);
    res.send(artists);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.post('/', [auth(), upload.single('image')], (req, res) => {
  const artistData = {
    name: req.body.name,
    user: req.user._id
  };

  if (req.body.description) {
    artistData.description = req.body.description;
  }

  if (req.file) {
    artistData.image = req.file.filename;
  }

  const artist = new Artist(artistData);

  artist.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth(), permit('admin')], (req, res) => {
  Artist.findById(req.params.id)
    .then(artist => {
      if (artist) {
        Artist.deleteOne({_id: artist._id})
          .then(result => res.send(result))
          .catch(() => res.sendStatus(500));
      } else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/:id/toggle_published', [auth(), permit('admin')], async (req, res) => {
  try {
    const artist = await Artist.findById(req.params.id);

    if (!artist) {
      return res.sendStatus(404);
    }

    artist.published = !artist.published;

    await artist.save();

    return res.send(artist);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;