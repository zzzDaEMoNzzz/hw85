const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: 'mongodb://localhost/music',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '474691306603779',
    appSecret: '5dc9d21417ba295587241430780957b1' // insecure!
  }
};
