const criteria = (req, res, next) => {
  let findCriterias = {published: true};

  if (req.user) {
    if (req.user.role === 'admin') {
      findCriterias = {};
    } else {
      findCriterias = {
        $or: [
          {published: true},
          {user: req.user._id}
        ]
      };
    }
  }

  req.criteria = findCriterias;

  next();
};

module.exports = criteria;